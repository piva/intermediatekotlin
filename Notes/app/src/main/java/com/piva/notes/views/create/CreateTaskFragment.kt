package com.piva.notes.views.create

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.piva.notes.R
import com.piva.notes.data.Task
import com.piva.notes.data.Todo
import com.piva.notes.views.foundation.ApplicationScope
import com.piva.notes.views.foundation.NullFieldChecker
import com.piva.notes.views.foundation.StateChangedTextWatcher
import com.piva.notes.views.tasks.CreateTodoView
import com.piva.notes.views.tasks.TaskLocalModel
import kotlinx.android.synthetic.main.fragment_create_task.*
import kotlinx.android.synthetic.main.view_create_task.view.*
import kotlinx.android.synthetic.main.view_create_todo.view.*
import toothpick.Toothpick
import javax.inject.Inject

private const val MAX_TODO_COUNT = 5

class CreateTaskFragment : Fragment() {

    @Inject
    lateinit var model: TaskLocalModel

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Toothpick.inject(this, ApplicationScope.scope)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        createTaskView.taskEditText.addTextChangedListener(object : StateChangedTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrEmpty() && previousValue.isNullOrEmpty()) {
                    addTodoView()
                }
                super.afterTextChanged(s)
            }
        })
    }

    private fun addTodoView() {
        if (canAddToDos()) {
            val view =
                (LayoutInflater.from(context).inflate(
                    R.layout.view_create_todo,
                    containerView,
                    false
                ) as CreateTodoView).apply {
                    todoEditTextView.addTextChangedListener(object : StateChangedTextWatcher() {
                        override fun afterTextChanged(s: Editable?) {
                            if (!s.isNullOrEmpty() && previousValue.isNullOrEmpty()) {
                                addTodoView()
                            } else if (!previousValue.isNullOrEmpty() && s.isNullOrEmpty()) {
                                removeTodoView(this@apply)

                                // max_todo_count will be 5 and something will be removed if count went from 6 to 5
                                if(containerView.childCount == MAX_TODO_COUNT){
                                    addTodoView()
                                }
                            }

                            super.afterTextChanged(s)
                        }
                    })
                }
            containerView.addView(view)
        }
    }

    private fun removeTodoView(view: View) {
        containerView.removeView(view)
    }

    private fun canAddToDos(): Boolean = (containerView.childCount < MAX_TODO_COUNT + 1)
            && !(containerView.getChildAt(containerView.childCount -1) as NullFieldChecker).hasNullField()
    private fun isTaskEmpty(): Boolean = createTaskView.taskEditText.editableText.isNullOrEmpty()

    fun saveTask(callback: (Boolean) -> Unit){
        createTask()?.let {
            model.addTask(it){
                callback.invoke(true) //TODO
            }
        } ?: callback.invoke(false)
    }

    fun createTask(): Task? {
        if( !isTaskEmpty()){
            containerView.run {

                var taskField: String? = null
                var todoList: MutableList<Todo> = mutableListOf()
                for (i in 0 until containerView.childCount){
                    if(i == 0){
                        taskField = containerView.getChildAt(i).taskEditText.editableText?.toString()
                    }else{

                        if(!containerView.getChildAt(i).todoEditTextView.editableText?.toString().isNullOrEmpty()){
                            todoList.add(
                                Todo(containerView.getChildAt(i).todoEditTextView.editableText.toString())
                            )
                        }

                    }
                }
                return taskField?.let {
                    Task(taskField, todoList)
                }
            }
        }else {
            return null
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance() = CreateTaskFragment()
    }
}
