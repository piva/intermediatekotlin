package com.piva.notes.views.foundation

interface NullFieldChecker {
    fun hasNullField(): Boolean

}