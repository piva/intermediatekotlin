package com.piva.notes.views.foundation

import com.piva.notes.views.notes.INoteModel
import com.piva.notes.views.notes.NoteLocalModel
import com.piva.notes.views.tasks.ITaskModel
import com.piva.notes.views.tasks.TaskLocalModel
import toothpick.Toothpick
import toothpick.config.Module

object ApplicationScope {
    val scope = Toothpick.openScope(this).apply {
        installModules(ApplicationModule)
    }
}

object ApplicationModule : Module() {
    init {
        bind(INoteModel::class.java).toInstance(NoteLocalModel())
        bind(ITaskModel::class.java).toInstance(TaskLocalModel())
    }
}