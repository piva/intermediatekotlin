package com.piva.notes.views.notes

import android.util.Log
import com.piva.notes.data.Note
import javax.inject.Inject

class NoteLocalModel @Inject constructor(): INoteModel{

    override fun getFakeData(): MutableList<Note> =
        mutableListOf(
            Note("Note 1"),
            Note("Note 2"),
            Note("Note 3"),
            Note("Note 4"),
            Note("Note 5"),
            Note("Note 6"),
            Note("Note 7")
        )

    override fun addNote(note: Note, callback: SuccessCallback) {
        Log.d("Note created", note.toString())
        callback.invoke(true)
    }

    override fun updateNote(note: Note, callback: SuccessCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteNote(note: Note, callback: SuccessCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveNotes(): List<Note> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



}