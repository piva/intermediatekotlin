package com.piva.notes.views.tasks

interface TaskListViewContract {

    fun onTodoUpdated(taskIndex: Int, todoIndex: Int, isCompleted: Boolean)

}