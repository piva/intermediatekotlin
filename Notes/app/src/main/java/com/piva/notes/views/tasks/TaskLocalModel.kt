package com.piva.notes.views.tasks

import android.util.Log
import com.piva.notes.data.Task
import com.piva.notes.data.Todo
import javax.inject.Inject

class TaskLocalModel @Inject constructor(): ITaskModel{

    override fun getFakeData(): MutableList<Task> = mutableListOf(
        Task(
            "Testing 1", mutableListOf(
                Todo("todo 1", false),
                Todo("todo 2")
            )
        ),
        Task(
            "Testing 2", mutableListOf(
                Todo("todo 1", true),
                Todo("todo 2")
            )
        ),
        Task(
            "Testing 3", mutableListOf(
                Todo("todo 1", false),
                Todo("todo 2")
            )
        ),
        Task(
            "Testing 4", mutableListOf(
                Todo("todo 1", false),
                Todo("todo 2")
            )
        )
    )

    override fun addTask(task: Task, callback: SuccessCallback) {
        Log.d("udemy", task.toString())
        callback.invoke(true)
    }

    override fun updateTask(task: Task, callback: SuccessCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteTask(task: Task, callback: SuccessCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveTasks(): List<Task> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}