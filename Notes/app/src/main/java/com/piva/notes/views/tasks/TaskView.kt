package com.piva.notes.views.tasks

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.piva.notes.R
import com.piva.notes.data.Task
import kotlinx.android.synthetic.main.item_task.view.*
import removeStrikeThrough
import setStrikeThrough

class TaskView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr) {

    lateinit var task: Task

    fun initView(task: Task, todoCheckedCallBack: (Int, Boolean) -> Unit) {
        this.task = task
        titleView.text = task.title

        task.todos.forEachIndexed { todoIndex, todo ->
            val todoView = (LayoutInflater.from(context).inflate(
                R.layout.view_todo,
                todoContainer,
                false
            ) as TodoView).apply {
                initView(todo) { isChecked ->

                    todoCheckedCallBack.invoke(todoIndex, isChecked)

                    if (isTaskComplete()) {
                        this@TaskView.titleView.setStrikeThrough()
                    } else {
                       this@TaskView.titleView.removeStrikeThrough()
                    }
                }
            }
            todoContainer.addView(todoView)
        }
    }

    private fun isTaskComplete(): Boolean = task.todos.filter { !it.isComplete }.isEmpty()



}